const colors = ["green", "red", "green", "blue","orange"];
const btn = document.getElementById("btn");
const section = document.getElementById("section");

btn.addEventListener("click", function () {
  const randomNumber = getRandomNumber();
  section.style.backgroundColor = colors[randomNumber];
});

function getRandomNumber() {
  return Math.floor(Math.random() * colors.length);
}

