//loops
//repeatedly run the block of code while condition true
//while loop
// let amount = 15;
// while (amount > 0) {
//   console.log(amount);
//   amount--;
// }

//do while
//code block fist,condition second
//run at least
// let money = 10;
// do {
//   console.log(`count ${money}`);
//   money++;
// } while (money < 10);

//for loops

// let run = 0;
// // run=initial value
// for (run; run < 10; run++) {
//   console.log("running addition" + run);
// }
// let newString = ["test", "test2"];
// for (let i = 0; i < newString.length; i++) {
//   console.log("running addition" + newString[i]);
// }
// newString.forEach((element) => {
//   console.log(element);
// });
// let newarr = ["test", "test2"];

//Array properties and methods
//length
// let students = ["john", "bob", "ben"];
// let students2 = ["john", "bob", "ben", "sdfsdf"];
// console.log(students2.length);

// console.log(students2[students2.length - 1]);
// const allName = students.concat(students2);
// console.log(students);
// console.log(allName);
// console.log(allName.reverse());

//concat

// const lastName = ["tester", "onion", "banana"];
// const allName = students.concat();
// console.log(allName.reverse());
// allName.unshift("susy");
// allName.unshift("ani");
// allName.shift();
// allName.shift();

//array and for loop

// let names = ["one", "two", "three", "four"];
// const addText = "new";
// let newArray = [];
// for (let i = 0; i < names.length; i++) {
//   let fullText = `${names[i]}${addText}`;
//   newArray.push(fullText);
// }

//refernce vs value

// let newvar = "kathir";
// let newvar2 = newvar;
// newvar2 = "ragu";
// console.log(newvar2);
// console.log(newvar);
let test = {
  name: "ragu",
};
let test2 = test;
test2.name = "kathir";
console.log(test2);
console.log(test);
